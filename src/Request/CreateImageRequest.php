<?php

namespace App\Request;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Validation;

class CreateImageRequest
{
    public function __construct(private array $values)
    {
        //
    }

    public static function fromRequest(Request $request): CreateImageRequest
    {
        return new CreateImageRequest([
            'title' => $request->get('title'),
            'image' => $request->files->get('image'),
        ]);
    }

    protected function getRules(): Assert\Collection
    {
        return new Assert\Collection([
            'image' => new Assert\Image([
                'maxSize' => '5M',
                'mimeTypes' => ['image/jpeg', 'image/png'],
            ]),
            'title' => [
                new Assert\NotBlank(),
                new Assert\Length(['max' => 100]),
            ],
        ]);
    }

    public function validate(): bool
    {
        $validator = Validation::createValidator();
        $validation = $validator->validate($this->values, $this->getRules());
        return $validation->count() === 0;
    }
}
