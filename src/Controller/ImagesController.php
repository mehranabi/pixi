<?php

namespace App\Controller;

use App\Entity\Image;
use App\Repository\ImageRepository;
use App\Request\CreateImageRequest;
use Exception;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Intervention\Image\ImageManagerStatic as ImageManager;

class ImagesController extends AbstractController
{
    public function index(ImageRepository $repository): Response
    {
        $images = $repository->all();
        $count = count($images);

        return $this->json([
            'message' => "There " . ($count > 1 ? 'are' : 'is') . " $count image(s) in the database.",
            'images' => $images,
        ]);
    }

    public function post(Request $request, ImageRepository $repository): Response
    {
        $valid = CreateImageRequest::fromRequest($request)->validate();
        if (!$valid) {
            return $this->json([
                'message' => 'Please check your input',
            ], 400);
        }

        $address = $this->storeImage($request->files->get('image'));
        if (!$address) {
            return $this->json([
                'message' => 'Cannot store image',
            ], 500);
        }

        $image = new Image();
        $image->setAddress($address);
        $image->setTitle($request->get('title'));

        try {
            $repository->add($image);
        } catch (Exception $_) {
            return $this->json([
                'message' => 'Cannot save image',
            ], 500);
        }

        return $this->json([
            'message' => 'The image has been saved',
        ], 201);
    }

    protected function storeImage(UploadedFile $file): ?string
    {
        $directory = $this->getParameter('images_dir');

        try {
            $name = md5($file->getClientOriginalName() . time()) . '.' . $file->getClientOriginalExtension();
            $file->move($directory, $name);

            ImageManager::make("$directory/$name")->resize(500, null, function ($constraint) {
                $constraint->aspectRatio();
            })->save("$directory/$name");

            return "$name";
        } catch (Exception $_) {
        }

        return null;
    }
}
