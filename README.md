# Pixi

Pixi is a simple Symfony app which let you upload images with a title.\

## TechStack

- Symfony 6
- Intervention Image

## How-To

There is only one endpoint available:

- `/images`
  Which can be called by GET or POST method, to perform a write or read action.

### Save an image

`POST /images`
By calling this endpoint with POST method you can create a new image record in the database.

**Parameters**:

| Parameter | Type | Required | Description     |
| --------- | ---- | -------- |-----------------|
| title | string | Yes | Max Length: 100 |
| image | file | Yes | Max Size: 5MB   |

### Read images

`GET /image`
By calling this endpoint with GET method you can read all the images in the database.

## Author
- S. Mehran Abghari
- mehran.ab80@gmail.com
